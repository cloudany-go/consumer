FROM golang:1.15

ARG VERSION=0.0.0

ENV CGO_ENABLED=0 GO111MODULE=on

WORKDIR /src
ADD go.mod /src
ADD go.sum /src

# cache go modules first
RUN go mod download

ADD . /src

RUN go build -ldflags="-s -w -X gitlab.com/cloudany-go/consumer/internal.version=${VERSION}" -o /app ./cmd/

FROM alpine:3.11
COPY --from=0 /app /usr/bin/app
ADD ./config.yml /app/config.yml
WORKDIR /app

CMD ["/usr/bin/app"]
