package main

import (
	"context"
	"os"

	"github.com/sirupsen/logrus"
	"gitlab.com/cloudany-go/consumer/internal"
)

func init() {
	if l, err := logrus.ParseLevel(os.Getenv("LOG_LEVEL")); nil == err {
		logrus.SetLevel(l)
	}
	logrus.SetFormatter(&logrus.TextFormatter{FullTimestamp: true})
}

func main() {
	s := internal.NewService()
	if err := s.Start(context.Background()); err != nil {
		logrus.WithError(err).Error("failed to start service")
	}
}
