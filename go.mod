module gitlab.com/cloudany-go/consumer

go 1.15

require (
	github.com/caarlos0/env/v6 v6.4.0
	github.com/gin-gonic/gin v1.6.3
	github.com/google/uuid v1.1.2
	github.com/sirupsen/logrus v1.7.0
	github.com/streadway/amqp v1.0.0
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776
)
