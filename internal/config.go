package internal

import (
	"sync"

	"github.com/caarlos0/env/v6"
	"gopkg.in/yaml.v3"
)

var (
	appCfg            appConfig
	loadAppConfigOnce sync.Once
)

type appConfig struct {
	Port       int    `env:"PORT" envDefault:"8088"`
	GrpcPort   int    `env:"GRPC_PORT" envDefault:"8089"`
	AmqpUrl    string `env:"AMQP_URL" envDefault:"amqp://guest:guest@rabbitmq:5672/"`
	ConfigFile string `env:"CONFIG_FILE" envDefault:"config.yml"`
}

type listenerConfig struct {
	Name    string   `yaml:"name"`
	Url     string   `yaml:"url"`
	Topics  []string `yaml:"topics"`
	Queue   string   `yaml:"queue"`
	Timeout int      `yaml:"timeout"`
}

type listenersConfig []*listenerConfig

func loadListenersConfig(data []byte) (listenersConfig, error) {
	var cfg listenersConfig
	err := yaml.Unmarshal(data, &cfg)
	return cfg, err
}

func getAppConfig() appConfig {
	loadAppConfigOnce.Do(func() {
		if err := env.Parse(&appCfg); nil != err {
			panic(err)
		}
	})
	return appCfg
}
