package internal

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/streadway/amqp"
	"gitlab.com/cloudany-go/consumer/internal/mq"
	"gitlab.com/cloudany-go/consumer/pkg/consumer"
)

func (s *Service) health(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"name":    name,
		"version": version,
	})
}

// publishHandler is a handler to publish payload message to broker
func (s *Service) publishHandler(c *gin.Context) {
	req := consumer.Message{}
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	p := &mq.Publishing{
		Topic: req.Topic,
		Payload: &amqp.Publishing{
			Body: []byte(req.Body),
		},
	}
	if req.ID != "" {
		p.Payload.Headers["x-request-id"] = req.ID
	} else {
		headerRequestID := c.GetHeader("x-request-id")
		if headerRequestID != "" {
			p.Payload.Headers["x-request-id"] = headerRequestID
		}
	}

	s.broker.Publish(p)
	c.Status(http.StatusOK)
}
