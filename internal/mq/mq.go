package mq

import (
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

var (
	DefaultExchangeName = "events"
)

// Listener describes a listener topic, queue, Handler
type Listener struct {
	Topic   string
	Queue   string
	Handler func(amqp.Delivery)
	Tag     string
}

type Publishing struct {
	Topic   string
	Payload *amqp.Publishing
}

type Broker interface {
	AddListener(*Listener)
	RemoveListener(*Listener)
	Connect() error
	Start(context.Context)
	Publish(*Publishing)
}

type brokerImpl struct {
	listeners      map[*Listener]bool
	conn           *amqp.Connection
	ch             *amqp.Channel
	exchange       string
	amqpUrl        string
	wg             sync.WaitGroup
	mux            sync.Mutex
	publishCh      chan *Publishing
	addListenersCh chan *Listener
	delListenersCh chan *Listener
}

func (b *brokerImpl) Connect() error {
	if b.conn != nil && !b.conn.IsClosed() {
		return nil
	}

	var err error
	dialConfig := amqp.Config{
		Heartbeat: time.Second * 30,
		Dial:      amqp.DefaultDial(time.Second * 5),
	}
	log := logrus.WithField("func", "mq.brokerImpl.Connect")
	if b.conn, err = amqp.DialConfig(b.amqpUrl, dialConfig); nil != err {
		return err
	}

	if b.ch, err = b.conn.Channel(); nil != err {
		log.WithError(err).Error("failed to create channel")
		return err
	}

	_ = b.ch.Qos(2, 0, true)

	err = b.ch.ExchangeDeclare(
		b.getExchangeName(),
		amqp.ExchangeTopic,
		true,  /* durable */
		false, /* auto delete */
		false, /* internal */
		false, /* no-wait */
		nil,   /* arguments */
	)
	if nil != err {
		log.WithError(err).Error("failed to declare exchange")
	}

	log.Debug("connected")
	return err
}

func (b *brokerImpl) getExchangeName() string {
	if "" == b.exchange {
		b.exchange = DefaultExchangeName
	}
	return b.exchange
}

func (b *brokerImpl) Publish(p *Publishing) {
	b.publishCh <- p
}

func (b *brokerImpl) publishDaemon() {
	defer b.wg.Done()
	log := logrus.WithField("func", "mq.brokerImpl.publishDaemon")
	for {
		p, ok := <-b.publishCh
		if !ok {
			log.Info("publishCh is closed, exiting daemon")
			return
		}

		if b.conn == nil || b.conn.IsClosed() {
			log.Warn("connection is closed, requeue & try next time")
			go func() {
				time.Sleep(time.Second)
				b.publishCh <- p
			}()
			continue
		}

		err := b.ch.Publish(b.getExchangeName(), p.Topic, false, false, *p.Payload)
		if nil != err {
			log.WithError(err).Error("failed to publish message")
		}
	}
}

func (b *brokerImpl) subscribe(l *Listener) {
	durable := len(l.Queue) > 0
	deleteUnused := !durable
	var (
		queue amqp.Queue
		err   error
		log   = logrus.WithField("func", "mq.brokerImpl.subscribe").WithField("topic", l.Topic)
	)

	log.Debug("subscribe topic")
	queue, err = b.ch.QueueDeclare(
		l.Queue,
		durable,
		deleteUnused,
		false,
		false,
		nil,
	)
	if nil != err {
		log.WithError(err).Error("failed to subscribe")
		return
	}

	// bind queue to exchange
	err = b.ch.QueueBind(queue.Name, l.Topic, b.getExchangeName(), false, nil)
	if nil != err {
		log.WithField("queue.Name", queue.Name).WithError(err).Error("failed to bind queue")
		return
	}

	// make a meaningful tag for consuming
	if "" == l.Tag {
		l.Tag = fmt.Sprintf("%s-%d-%s", l.Topic, time.Now().Unix(), uuid.New().String())
	}

	var msgCh <-chan amqp.Delivery
	msgCh, err = b.ch.Consume(
		queue.Name,
		l.Tag,
		false, /* auto ack */
		false, /* exclusive */
		false, /* no local */
		false, /* no wait */
		nil,
	)
	if nil != err {
		log.WithField("queue.Name", queue.Name).WithError(err).Error("failed to consume")
		return
	}
	b.wg.Add(1)
	b.mux.Lock()
	b.listeners[l] = true
	b.mux.Unlock()

	go b.startListener(l, msgCh)
}

func (b *brokerImpl) startListener(l *Listener, msgCh <-chan amqp.Delivery) {
	defer b.wg.Done()

	for {
		m, ok := <-msgCh
		if !ok {
			return
		}

		l.Handler(m)
	}
}

func (b *brokerImpl) AddListener(l *Listener) {
	b.mux.Lock()
	defer b.mux.Unlock()

	subscribed, exists := b.listeners[l]
	if !exists {
		b.listeners[l] = false
	}

	if !subscribed {
		b.addListenersCh <- l
	}
}

func (b *brokerImpl) RemoveListener(l *Listener) {
	var exists bool
	b.mux.Lock()
	_, exists = b.listeners[l]
	b.mux.Unlock()
	if exists {
		b.delListenersCh <- l
	}
}

func (b *brokerImpl) addListenersDaemon() {
	logrus.Debug("start addListenersDaemon")
	defer b.wg.Done()
	for {
		l, ok := <-b.addListenersCh
		if !ok {
			return
		}

		b.mux.Lock()
		subscribed := b.listeners[l]
		b.mux.Unlock()

		if !subscribed {
			b.subscribe(l)
		}
	}
}

func (b *brokerImpl) Start(ctx context.Context) {
	log := logrus.WithField("func", "mq.brokerImpl.Start")

	reconnectDelay := time.Second * 3
	closeErrCh := make(chan *amqp.Error)

	err := b.Connect()
	if nil != err {
		log.WithError(err).Error("failed to connect to broker")
		time.Sleep(reconnectDelay)
		b.Start(ctx)
		return
	}
	log.Debug("subscribe listeners")
	// start subscribing
	b.addListenersCh = make(chan *Listener, 1024)
	b.wg.Add(1)
	go b.addListenersDaemon()
	// add all listeners to subscribing queue
	for l := range b.listeners {
		b.addListenersCh <- l
	}

	b.wg.Add(1)
	go b.publishDaemon()

	// register close hook
	b.conn.NotifyClose(closeErrCh)

	for {
		select {
		case <-ctx.Done():
			log.Debug("connection is closing")
			return

		case err, errChStillOpen := <-closeErrCh:
			if !errChStillOpen {
				log.Debug("normal shutdown")
				return // closed connection, it's fine
			}
			log.WithError(err).Error("connection is closed, reconnecting ...")

			// remove all handlers
			b.mux.Lock()
			for l := range b.listeners {
				b.listeners[l] = false
			}
			b.mux.Unlock()
			time.Sleep(reconnectDelay)
			b.Start(ctx)
		}
	}
}

func NewBroker(amqpUrl string) Broker {
	return &brokerImpl{
		amqpUrl:        amqpUrl,
		listeners:      make(map[*Listener]bool),
		addListenersCh: make(chan *Listener, 1024),
		delListenersCh: make(chan *Listener, 1024),
		publishCh:      make(chan *Publishing, 1024),
	}
}
