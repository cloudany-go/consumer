package internal

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/cloudany-go/consumer/internal/mq"
	"gitlab.com/cloudany-go/consumer/pkg/consumer"
)

var (
	name    = "consumer"
	version = "v0.0.0"
)

type Service struct {
	router *gin.Engine
	wg     sync.WaitGroup
	broker mq.Broker
	ctx    context.Context
}

func NewService() *Service {
	return &Service{}
}

func (s *Service) Start(ctx context.Context) error {
	var cancel context.CancelFunc
	l := logrus.WithField("func", "Service.Start")
	s.ctx, cancel = context.WithCancel(ctx)
	defer cancel()
	httpLis, err := net.Listen("tcp", fmt.Sprintf("0.0.0.0:%d", getAppConfig().Port))
	if nil != err {
		return err
	}
	s.router = gin.New()
	s.router.GET("/", s.health)
	s.router.POST("/events", s.publishHandler)

	// start amqp consumer
	s.broker = mq.NewBroker(getAppConfig().AmqpUrl)
	if err := s.broker.Connect(); err != nil {
		return err
	}

	s.wg.Add(1)
	go func() {
		defer s.wg.Done()

		s.broker.Start(s.ctx)
	}()

	s.loadListeners()

	httpServer := &http.Server{
		Handler: s.router,
	}

	// start http listener
	s.wg.Add(1)
	go func() {
		defer s.wg.Done()
		l.WithField("addr", httpLis.Addr().String()).Info("[http] listening")
		if err := httpServer.Serve(httpLis); err != nil && err != http.ErrServerClosed {
			l.WithError(err).Error("failed to serve HTTP")
		}
	}()

	// start interrupt listen
	s.wg.Add(1)
	go func() {
		defer s.wg.Done()
		s := make(chan os.Signal, 1)
		signal.Notify(s, syscall.SIGHUP, syscall.SIGTERM, syscall.SIGINT)

		select {
		case <-s:
		case <-ctx.Done():
		}

		_ = httpServer.Close()
		cancel()
	}()

	s.wg.Wait()
	return nil
}

func (s *Service) loadListeners() {
	byt, err := ioutil.ReadFile(getAppConfig().ConfigFile)
	if err != nil {
		logrus.
			WithField("file", getAppConfig().ConfigFile).
			WithError(err).Error("failed to load listeners config")
		return
	}

	cfgListeners, err := loadListenersConfig(byt)
	if err != nil {
		logrus.WithError(err).Error("failed to decode listeners config")
		return
	}

	for _, cfg := range cfgListeners {
		handler := s.makeHandler(cfg)
		for _, topic := range cfg.Topics {

			l := &mq.Listener{
				Topic:   topic,
				Queue:   cfg.Queue,
				Handler: handler,
				Tag:     fmt.Sprintf("%s:%s-%s", cfg.Name, topic, uuid.New().String()),
			}
			logrus.WithField("topic", topic).
				WithField("tag", l.Tag).
				Debug("add listener")
			s.broker.AddListener(l)
		}
	}
}

func (s *Service) makeHandler(cfg *listenerConfig) func(amqp.Delivery) {

	if cfg.Timeout == 0 {
		cfg.Timeout = 5000
	}
	client := &http.Client{
		Timeout: time.Millisecond * time.Duration(cfg.Timeout),
	}
	makeRequest := func(m consumer.Message) *http.Request {
		byt, _ := json.Marshal(m)
		req, _ := http.NewRequest(http.MethodPost, cfg.Url, bytes.NewReader(byt))

		req.Header.Set("content-type", "application/json")
		req.Header.Set("user-agent", "consumer/v1.0.0")

		return req
	}

	log := logrus.WithField("func", "Service.handler")

	return func(m amqp.Delivery) {
		_ = m.Ack(false)

		log.WithField("body", string(m.Body)).Debug("delivering")

		xid, ok := m.Headers["x-request-id"]
		if !ok {
			xid = uuid.New().String()
		}
		message := consumer.Message{
			Topic: m.RoutingKey,
			Body:  string(m.Body),
			ID:    xid.(string),
		}
		req := makeRequest(message)
		rsp, err := client.Do(req)
		if err != nil {
			log.WithError(err).Error("failed to request upstream")
			return
		}
		var byt []byte
		if rsp.Body != nil {
			defer func() { _ = rsp.Body.Close() }()
			byt, _ = ioutil.ReadAll(rsp.Body)
		}

		if rsp.StatusCode != http.StatusOK {
			log.WithField("status", rsp.StatusCode).WithField("body", string(byt)).Error("invalid response code")
			return
		}

		log.Debug("push to upstream successfully")
	}
}
