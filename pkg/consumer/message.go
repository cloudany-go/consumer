package consumer

type Message struct {
	Topic string `json:"topic" binding:"required"`
	ID    string `json:"id"`
	Body  string `json:"body" binding:"required"`
}
