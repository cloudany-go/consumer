# consumer

## Build

```
go build -ldflags="-X 'gitlab.com/cloudany-go/consumer/internal.version=v1.0.0'" -o server ./cmd/
```

## Architecture

![img](https://st.phuong.io/architecture.png)

## Publish events

To remove dependencies of other services on RabbitMQ,`#consumer` provides an HTTP interface to publish event

```http
POST /events HTTP/1.1
Host: http://consumer
Content-Type: application/json

{
    "topic":"echo.test",
    "body": "hello world (deliver via consume)"
}
```

## Consume events

This is consumer primary responsibility. It helps to deliver events what were published from above step to target services
what are interested in.
To listen a topic (you also call event), you'll need to register your service interesting
with consumer in configuration file (`config.yml`)
Each service registers itself to consumer has create an item in services attributes, below is a sample for echo service
register to listen to event user.profile_updated

```yaml
- name: echo
  url: https://enpnpxoahtzqhhd.m.pipedream.net
  topics:
    - user.profile_updated
  queue: echo:user.profile_updated

```

*note*: all topics in the same queue will be consumed sequentially (FIFO)
